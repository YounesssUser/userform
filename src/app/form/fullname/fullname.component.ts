import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-fullname',
  templateUrl: './fullname.component.html',
  styleUrls: ['./fullname.component.css']
})
export class FullnameComponent implements OnInit {
  patter = /^[a-zA-Z ]+$/;
  fullName:string = "";
  @Input() isDesibled: boolean;
  @Output() fullNameEvent = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
    document.getElementById("fullname-validation").style.display = "none";
  }
  
  onFocusOut(){
    if(this.patter.test(this.fullName)){
      this.sendMessage();
      document.getElementById("fullname-validation").style.display = "none";
    }else{
      console.log("wk")
      document.getElementById("fullname-validation").style.display = "inline-block";
    }
  }

  sendMessage(){
    this.fullNameEvent.emit("fullname");
  }
}
