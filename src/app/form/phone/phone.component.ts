import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-phone',
  templateUrl: './phone.component.html',
  styleUrls: ['./phone.component.css']
})
export class PhoneComponent implements OnInit {
  @Input() isDesibled: boolean;
  patter = /^[0-9() ]+$/;
  phone:string = "";
  @Output() phoneEvent = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
    document.getElementById("phone-validation").style.display = "none";
  }

  onFocusOut(){
    if(this.patter.test(this.phone)){
      this.sendMessage();
      document.getElementById("phone-validation").style.display = "none";
    }else{
      document.getElementById("phone-validation").style.display = "inline-block";
    }
  }

  sendMessage(){
    this.phoneEvent.emit("phone");
  }

}
