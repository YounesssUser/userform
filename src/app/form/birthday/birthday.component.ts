import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-birthday',
  templateUrl: './birthday.component.html',
  styleUrls: ['./birthday.component.css']
})
export class BirthdayComponent implements OnInit {
  birthday:string = "";
  patter = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/g;
  @Input() isDesibled: boolean;
  @Output() birthdayEvent = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
    document.getElementById("birtday-validation").style.display = "none";
  }

  onFocusOut(){
    if(this.patter.test(this.birthday)){
      this.sendMessage();
      document.getElementById("birtday-validation").style.display = "none";
    }else{
      document.getElementById("birtday-validation").style.display = "inline-block";
    }
  }

  sendMessage(){
    this.birthdayEvent.emit("birthday");
  }
}
