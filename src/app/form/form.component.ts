import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  displayName:boolean = false;
  displayBirhtDay:boolean = true;
  displayPhone:boolean = true;
  displaySubmitBtn:boolean = true;

  constructor() { }

  ngOnInit() {
  }
  
  gotoNextStep($event){
    console.log("in gotoNextStep");
    console.log("event ==> " + $event);
    
    
    if($event === "fullname"){
      this.displayName = true;
      this.displayBirhtDay = false;
    }else if($event === "birthday"){
      this.displayBirhtDay = true;
      this.displayPhone = false;
    }
    else if($event === "phone"){
      this.displayPhone = true;
      this.displaySubmitBtn = false;
    }
  }

  emptyForm(){
    
  }
}
